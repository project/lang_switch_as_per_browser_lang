var locale = navigator.language || navigator.userLanguage;

var url = jQuery('a.modal-detect-lang').attr( 'href' );
jQuery('a.modal-detect-lang').attr( 'href', url + '?lang=' + locale )

setTimeout(
  function() 
  {
	  if( 'en-GB' != localStorage.getItem("lang") ) {
		jQuery('a.modal-detect-lang').click();
	  }
	  localStorage.setItem("lang", locale);
}, 400);

