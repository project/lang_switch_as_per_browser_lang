<?php

/**
 * @file
 * CustomModalController class.
 */

namespace Drupal\lang_switch_as_per_browser_lang\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;

class CustomModalController extends ControllerBase {

  public function modal() {

    $lang = $_GET['lang'];

    $options = [
      'dialogClass' => 'popup-dialog-class',
      'width' => '50%',
    ];


    $langcd = \Drupal::languageManager()->getLanguages();
    $langcd = array_keys($langcd);

    global $base_url;

    $link = '';
    foreach( $langcd as $lang ) {
      if( $lang != 'en' ) {
        $link .= "<a href='$base_url/$lang'>$lang</a><br>";
      } else {
        $link .= "<a href='$base_url'>$lang</a><br>";
      }
    }

    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand(t('Modal title' . $lang), t('Please select lang:- '. $link), $options));

    return $response;
  }
}